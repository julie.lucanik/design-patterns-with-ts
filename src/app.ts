/**
---------- ADAPTER PATTERN ----------
 * Adapter pattern  translate the interface of one class into another interface.
  This means that we can make classes work together that couldn’t otherwise because of incompatible interfaces.
 */

import { RecordHandler, loader } from "./loader";

type Listener<EventType> = (ev: EventType) => void;
// observer
function createObserver<EventType>(): {
  subscribe: (listener: Listener<EventType>) => () => void;
  publish: (event: EventType) => void;
} {
  let listeners: Listener<EventType>[] = [];

  return {
    subscribe: (listener: Listener<EventType>): (() => void) => {
      listeners.push(listener);
      // unsubscribe return (like useEffect)
      return () => (listeners = listeners.filter((l) => l !== listener));
    },
    publish: (event: EventType) => {
      listeners.forEach((l) => l(event));
    },
  };
}

interface BeforeSetEvent<T> {
  value: T;
  newValue: T;
}

interface AfterSetEvent<T> {
  value: T;
}

interface Pokemon {
  id: string;
  attack: number;
  defense: number;
}

interface BaseRecord {
  id: string;
}

interface Database<T extends BaseRecord> {
  set(newValue: T): void;
  get(id: string): void;

  onBeforeAdd(listener: Listener<BeforeSetEvent<T>>): () => void;
  onAfterAdd(listener: Listener<AfterSetEvent<T>>): () => void;

  visit(visitor: (item: T) => void): void;
  selectBest(scoringStrategy: (item: T) => number): T | undefined;
}

// Factory
function createDatabase<T extends BaseRecord>() {
  class InMemoryDatabase implements Database<T> {
    private db: Record<string, T> = {};
    // Singleton - to ensure there will be only only one database creation
    static instance: InMemoryDatabase = new InMemoryDatabase();

    private beforeAddListeners = createObserver<BeforeSetEvent<T>>();
    private afterAddListeners = createObserver<AfterSetEvent<T>>();

    public set(newValue: T): void {
      this.beforeAddListeners.publish({
        newValue,
        value: this.db[newValue.id],
      });
      this.db[newValue.id] = newValue;

      this.afterAddListeners.publish({ value: newValue });
    }

    public get(id: string): T {
      return this.db[id];
    }

    onBeforeAdd(listener: Listener<BeforeSetEvent<T>>): () => void {
      return this.beforeAddListeners.subscribe(listener);
    }
    onAfterAdd(listener: Listener<AfterSetEvent<T>>): () => void {
      return this.afterAddListeners.subscribe(listener);
    }

    // Visit
    visit(visitor: (item: T) => void): void {
      Object.values(this.db).forEach(visitor);
    }

    // Strategy
    selectBest(scoringStrategy: (item: T) => number): T | undefined {
      const found: {
        max: number;
        item: T | undefined;
      } = {
        max: 0,
        item: undefined,
      };

      Object.values(this.db).reduce((acc, item) => {
        const score = scoringStrategy(item);
        if (score >= acc.max) {
          acc.max = score;
          acc.item = item;
        }

        return acc;
      }, found);

      return found.item;
    }
  }

  // Singleton - OR
  // const db = new InMemoryDatabase()
  // return db
  return InMemoryDatabase;
}

const PokemonDB = createDatabase<Pokemon>();

// Adapter pattern
class PokemonDBAdapter implements RecordHandler<Pokemon> {
  addRecord(record: Pokemon) {
    PokemonDB.instance.set(record);
  }
}

const unsubscribe = PokemonDB.instance.onAfterAdd(({ value }) =>
  console.log(value)
);

PokemonDB.instance.set({ id: "Bulbasaur", attack: 69, defense: 48 });

PokemonDB.instance.set({ id: "Squirtle", attack: 85, defense: 29 });

loader("./data.json", new PokemonDBAdapter());

unsubscribe();

PokemonDB.instance.set({ id: "Abra", attack: 43, defense: 75 });

const bestDefensive = PokemonDB.instance.selectBest(({ defense }) => defense);
const bestAttack = PokemonDB.instance.selectBest(({ attack }) => attack);

// console.log(PokemonDB.instance.get("Bulbasaur"));
console.log(`the deadliest Pokemon is ${bestAttack?.id}`);
console.log(`the most protector Pokemon is ${bestDefensive?.id}`);
